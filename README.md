# Recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage

### Environment Variables

 * `LISTEN_PORT` - Port to listen on (default: `8000`)
 * `APP_HOST` - Hostname of thr app
 * `APP_PORT` - Port